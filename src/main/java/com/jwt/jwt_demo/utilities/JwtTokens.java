package com.jwt.jwt_demo.utilities;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class JwtTokens {

    private final String authToken;
    private final String refreshToken;
    private final String username;

}
