package com.jwt.jwt_demo.utilities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RefreshTokenModel {
    private String oldauthToken;
    private String refreshToken;
}
