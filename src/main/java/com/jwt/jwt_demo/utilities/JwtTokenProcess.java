package com.jwt.jwt_demo.utilities;

import io.jsonwebtoken.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class JwtTokenProcess {
    private String secret = "jwt_auth";
    private String refreshSecret = "jwt_auth_refresh";

    public String generateAuthJwtToken(String username) {
        return Jwts.builder().setSubject("Auth" + username).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 900000))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public String generateRefreshJwtToken(String username) {
        return Jwts.builder().setSubject("Refresh" + username).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 7200000))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public String generateAuthTokenFromRefresh(String rtoken) {

        String userName = Jwts.parser().setSigningKey(refreshSecret).parseClaimsJws(rtoken).getBody().getSubject();

        return Jwts.builder()
                .setSubject(new StringBuilder(userName).reverse().toString())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + 7200000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    private boolean validateToken(String authToken, String jwtRefreshSecret, boolean secret) {
        try {
            Jwts.parser().setSigningKey(jwtRefreshSecret).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException e) {
            System.err.println("Invalid JWT " + (secret ? "secret " : "") + "token: {}" + e.getMessage());
        } catch (ExpiredJwtException e) {
            System.err.println("JWT " + (secret ? "secret " : "") + "token is expired: {}" + e.getMessage());
        } catch (UnsupportedJwtException e) {
            System.err.println("JWT " + (secret ? "secret " : "") + "token is unsupported: {}" + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.err.println("JWT " + (secret ? "secret " : "") + "claims string is empty: {}" + e.getMessage());
        }
        return false;
    }

    public boolean validateJwtRefreshToken(String refreshToken) {
        return validateToken(refreshToken, secret, true);
    }

    public JwtTokens refreshToken(RefreshTokenModel refreshTokenModel) {

        String refreshToken = refreshTokenModel.getRefreshToken();

        if (validateJwtRefreshToken(refreshToken)) {
            String newToken = generateAuthTokenFromRefresh(refreshToken);
            //TODO : revoyer le auth token à partir du refresh token
//            return new JwtTokens(
//                    newToken
//            );
        }
        return null;
    }


}
