package com.jwt.jwt_demo.controller;


import com.jwt.jwt_demo.entity.AuthRequest;
import com.jwt.jwt_demo.entity.RefreshRequest;
import com.jwt.jwt_demo.utilities.JwtTokenProcess;
import com.jwt.jwt_demo.utilities.JwtTokens;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/auth")
public class AuthentificationController {

    @Autowired
    JwtTokenProcess jwtTokenProcess;

    @Autowired
    AuthenticationManager authenticationManager;

    @GetMapping("/")
    public ResponseEntity<String> Hello() {
        return ResponseEntity.ok("Hello, you!");
    }

    @PostMapping("/signin")
    public ResponseEntity<String> generateTokenEndPoint(@RequestBody AuthRequest authRequest) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (Exception e) {
            throw new Exception("Invalid username or password");
        }

        String signInToken = jwtTokenProcess.generateAuthJwtToken(authRequest.getUsername());
        String refreshToken = jwtTokenProcess.generateRefreshJwtToken(authRequest.getUsername());

        return ResponseEntity.ok(signInToken + "+" + refreshToken);
    }

    @PostMapping("/refresh")
    public String generateRefTokenEndPoint(@RequestBody RefreshRequest refreshRequest) throws Exception {
//        JwtTokens tokens = authServices.refreshToken(tokenForm);
//        if (jwtRefreshDto == null) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
//        return new ResponseEntity<>(jwtRefreshDto, HttpStatus.CREATED);


        return null;
    }

}
