package com.jwt.jwt_demo;

import com.jwt.jwt_demo.entity.User;
import com.jwt.jwt_demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@EntityScan(basePackages = "com.jwt.jwt_demo.entity")
public class JwtDemoApplication {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostConstruct
	public void initUsers() {

		//Initializing the list of users
		List<User> users = Stream.of(
				new User("1001@mail.com", "user1001", passwordEncoder.encode("password1001")),
				new User("1002@mail.com", "user1002", passwordEncoder.encode("password1002")),
				new User("1003@mail.com", "user1003", passwordEncoder.encode("password1003")),
				new User("1004@mail.com", "user1004", passwordEncoder.encode("password1004")),
				new User("1005@mail.com", "user1005", passwordEncoder.encode("password1005"))
		).collect(Collectors.toList());

		//Saving the list of users
		userRepository.saveAll(users);
	}

	public static void main(String[] args) {
		SpringApplication.run(JwtDemoApplication.class, args);
	}

}
